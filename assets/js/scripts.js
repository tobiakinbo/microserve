
$( document ).ready(function() {
	  /* eliminate affix jump */
    $('.navbar-wrapper').height($(".navbar").height());

    new WOW().init();

});


$(window).load(function() {

 /* activate jquery isotope */
  var $container = $('#posts').isotope({
    itemSelector : '.item',
    isFitWidth: true

  });

 $container.isotope({
  getSortData : {
       date : function ( $container ) {
      return $container.find('.date').attr('data-date');
     }
    },
    sortBy : 'date',
    sortAscending : true

  });

  $container.isotope({ filter: '*' });


    // filter items on button click
  $('#filters').on( 'click', 'button', function() {
    var filterValue = $(this).attr('data-filter');
    $container.isotope({ filter: filterValue });
    console.log('working');
  });
});

